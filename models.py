from pydantic import BaseModel
from typing import Optional

class MovieList(BaseModel):
    title:Optional [str] = None
    stars:Optional [str] = None 
    genre:Optional [str] = None 

class MovieEntry(BaseModel):
    title:Optional [str] = None
    stars:Optional [str] = None
    genre:Optional [str] = None

class MovieUpdate(BaseModel):
    title:Optional [str] = None
    stars:Optional [str] = None
    genre:Optional [str] = None

class MovieDelete(BaseModel):
    title:Optional [str] = None
    stars:Optional [str] = None
    genre:Optional [str] = None

   