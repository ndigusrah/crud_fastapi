from sqlalchemy.engine import result
from fastapi import FastAPI, Depends
from fastapi.param_functions import Body
from sqlalchemy import engine
from schemas import *
from typing import List
from models import *
from config import *
from connect import *
from redis_adapter import RedisAdapter
import uvicorn
import json

app = FastAPI()

redis = RedisAdapter(
    dict(
        host= "10.10.19.43",
        port= 6379,
        db= 0,
        password= None
    ),
    prefix="redis",
    app=app
)

@app.post('/create', response_model=MovieList, responses={204: {"model": None}})
async def create_Movie(st: MovieEntry = Body(...)):
    query=movies.insert().values(
        title=st.title,
        stars=st.stars,
        genre=st.genre
    )
    engine.execute(query)
    return {'title' : st.title,
        'stars':st.stars,
        'genre':st.genre
        }

@app.get('/read')
async def List_movies():
    redis_key = "movies"
    if redis.exists (redis_key):
        result = redis.get(redis_key)
        print ("Success")
        return result
    query=movies.select()
    datas = await database.fetch_all(query)
    """
    [(a,b,c),(a,b,c),(a,b,c),(a,b,c)]
    """
    result = []
    for data in datas:
        result.append(
            {
                "title": data.title,
                "stars": data.stars,
                "genre": data.genre
            }
        )
    print (result)
    redis.set(redis_key, result, 300)
    return result

@app.put('/update', response_model=MovieList)
async def update_movie(us:MovieUpdate):
    query=movies.update().\
    where(movies.c.title==us.title).\
    values(
        title=us.title,
        stars=us.stars,
        genre=us.genre
    )
    await database.execute(query)
    return {'title' : us.title,
        'stars':us.stars,
        'genre':us.genre
        }

@app.post('/search', response_model=List[MovieList], responses={204: {"model": None}})
async def Search_Movie_by_stars(st: MovieEntry = Body(...)):
    query=movies.select().\
    where(movies.c.stars.like('%' + st.stars + '%'))
    return await database.fetch_all(query)


@app.delete("/delete")
async def delete_movie(sd:MovieDelete):
    query=movies.delete().where(movies.c.title==sd.title)
    await database.execute(query)
    return {
        'title' : sd.title,
        'stars':sd.stars,
        'genre':sd.genre
       
        }

if __name__ == '__main__':
    uvicorn.run(
        'app:app',
        host='0.0.0.0',
        port= 5000,
        debug= True,
        reload=True
    )
